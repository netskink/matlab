% question 8

% Load Images
I18=imread('frame_1.jpg');
I1D=im2double(I18);

% verify size
[nRow,nCol] = size(I1D);
if 352 ~= nCol
    disp('number of columns for I1D is wrong');
end
if 288 ~= nRow
    disp('number of rows for I1D is wrong');
end

I28=imread('frame_2.jpg');
I2D=im2double(I28);

% verify size
[nRow,nCol] = size(I2D);
if 352 ~= nCol
    disp('number of columns for I2D is wrong');
end
if 288 ~= nRow
    disp('number of rows for I2D is wrong');
end


% I1 corresponds to frame 1
% I2 corresponds to frame 2 (the next frame in time)

% consider the 32x32 block in I2 that has upper left
% corner (65,81) and lower right corner (96,112)
% first number is row index from 1 to 288
% second number is column index from 1 to 352

% Denote the target block by BTarget
ulcx=65;
ulcy=81;
lrcx=ulcx+32-1;
lrcy=ulcy+32-1;
BTarget = I2D(ulcx:lrcx, ulcy:lrcy);
% Verify we have the submatrix correct
% Belt
for i = 65:96
    for j = 81:112
        if BTarget(i-65+1,j-81+1) ~= I2D(i,j)
            disp('Error: BTarget has wrong value');
        end
    end
end
% Suspenders
if BTarget ~= I2D(ulcx:lrcx, ulcy:lrcy)
    disp('Error: BTarget has wrong value');
end    
            

% Find the 32x32 block in I1 which is most similar
% to BTarget
MinError = 1E6;
FoundX=1; % corresponds to upper left X of image
FoundY=1; % corresponds to upper left Y of image
for i = 1:nRow-32
    for j = 1:nCol-32
        theError = MAE( BTarget, I1D(i:i+32-1, j:j+32-1) );
        if theError <= MinError
            MinError = theError;
            FoundX = i;
            FoundY = j;
            disp(['Found a candidate frame. MAE = ', num2str(theError)]);
        end
    end
end
% The final MAE is a multiple of 255.
MinError = MinError * 255;
MinError=roundn(MinError,-2);
% Display result for hw submission
disp([num2str(FoundX), ' ', num2str(FoundY), ' ', num2str(MinError)]);

figure('Name','Frame 1');
imshow(I1D);
figure('Name','Frame 2');
imshow(I2D);
figure('Name','Frame 2 Target Portion');
imshow(BTarget);
figure('Name', 'Diff of Target Frame2 Portion and Found Portion in Frame1');
imshowpair(BTarget, I1D(FoundX:FoundX+32-1, FoundY:FoundY+32-1), 'diff');
figure('Name', 'Diff of Target Frame2 Portion and Found Portion in Frame1. Color');
imshowpair(BTarget, I1D(FoundX:FoundX+32-1, FoundY:FoundY+32-1));

% Use Mean-absolute-error (MAE) as the matching criterion

% MAE(B1,B2) = 1/(MxN) sum i=1 to M sum j=1 N abs( B1(i,j)-B2(i,j) )

% Test code for my MAE function.
%A=[1,2,3; 4,5,6; 7,8,9]
%B=[2,3,4; 5,6,7; 8,9,10]
%result=MAE(A,B) % should be 1

%A=[1,2,3; 4,5,6; 7,8,9]
%B=[1,2,3; 4,5,6; 7,8,9]
%result=MAE(A,B) % should be 0


%A=[1,2,3; 4,5,6; 7,8,9]
%B=[2,2,3; 4,5,6; 7,8,9]
%result=MAE(A,B) % should be 1/9