
cat ~/.local/share/applications/matlab.desktop

#!/usr/bin/env xdg-open
[Desktop Entry]
Encoding=UTF-8
Version=1.0
Type=Application
#Terminal=true
Terminal=false
Exec=/usr/local/bin/MATLAB/matlab -desktop -prefersoftwareopengl
Name[en_US]=matlab
GenericName=matlab app
Icon[en_US]=/usr/local/MATLAB/R2021b/bin/glnxa64/cef_resources/matlab_icon.png
Categories=Application

# interesting
#Exec=env MESA_LOADER_DRIVER_OVERRIDE=i965 /usr/local/bin/matlab -desktop -prefersoftwareopengl
